import CanvasJSStockChart from '@canvasjs/vue-stockcharts';

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(CanvasJSStockChart);
});