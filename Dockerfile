ARG NODE_VERSION=20.10.0

FROM node:${NODE_VERSION}-slim as base

ARG PORT=3000

WORKDIR /app

FROM base as build

COPY package.json .

RUN npm install

COPY . .

RUN npm run build

FROM base

ENV PORT=$PORT
ENV NODE_ENV=production

COPY --from=build /app/.output /app/

CMD [ "node", "server/index.mjs" ]