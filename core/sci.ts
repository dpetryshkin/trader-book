import {
  chartBuilder,
  CustomAnnotation, easing,
  EAxisAlignment,
  EAxisType,
  EChart2DModifierType,
  EHorizontalAnchorPoint,
  EVerticalAnchorPoint,
  EXyDirection, HorizontalLineAnnotation,
  type I2DSubSurfaceOptions, NumberRange,
  Rect,
  SciChartVerticalGroup,
  Thickness
} from 'scichart'
// import { multiPaneData } from '@/lib/multiPaneData'
import { appTheme } from '@/lib/theme'
import { getTooltipLegendTemplate } from '@/utils/legends/TooltipLegend'
import { chart } from '@/core/chart'
import indicatorsList from '@/core/indicators'
import type { IndicatorData } from '@/interfaces/indicator'
import type { TPriceBar } from '@/api/binance/restClient'
import { LARGE_TRADE_THRESHOLD } from '@/core/createChart'

export const mainChartWrapper2 = 'cc_chart2'
export const subChartWrapper1 = 'subChartWrapper1'
export const subChartWrapper2 = 'subChartWrapper2'
export const containerId2 = 'containerId2'

export const draw = async (priceBars: TPriceBar[], indicators: IndicatorData[]) => {
  const verticalGroup = new SciChartVerticalGroup()
  const xValues: number[] = [];
  const openValues: number[] = [];
  const highValues: number[] = [];
  const lowValues: number[] = [];
  const closeValues: number[] = [];
  const volumeValues: number[] = [];
  priceBars.forEach((priceBar: TPriceBar) => {
    xValues.push(priceBar.date);
    openValues.push(priceBar.open);
    highValues.push(priceBar.high);
    lowValues.push(priceBar.low);
    closeValues.push(priceBar.close);
    volumeValues.push(priceBar.volume);
  });

  const axisAlignment = EAxisAlignment.Right

  const commonSubChartSurfaceOptions: I2DSubSurfaceOptions = {
    subChartPadding: Thickness.fromNumber(10),
    isTransparent: false,
    theme: appTheme.SciChartJsTheme
  }

  const subChartModifiers = [
    { type: EChart2DModifierType.ZoomPan, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.PinchZoom, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.ZoomExtents, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.MouseWheelZoom, options: { xyDirection: EXyDirection.XDirection } },
    {
      type: EChart2DModifierType.Rollover,
      options: { modifierGroup: 'cursorGroup', showTooltip: false, tooltipLegendTemplate: getTooltipLegendTemplate }
    }
  ]

  const config = {
    upCol: appTheme.VividGreen,
    downCol: appTheme.MutedRed,
    opacity: 'AA'
  }

  const { sciChartSurface: mainSurface, wasmContext } = await chartBuilder.build2DChart(mainChartWrapper2, {
    surface: {
      id: 'mainSurface',
      theme: appTheme.SciChartJsTheme
    },
    xAxes: {
      type: EAxisType.NumericAxis,
      options: {
        isVisible: false
      }
    },
    yAxes: {
      type: EAxisType.NumericAxis,
      options: {
        isVisible: false
      }
    },
    subCharts: [
      chart(xValues, openValues, highValues, lowValues, closeValues, volumeValues, axisAlignment, subChartModifiers, config, commonSubChartSurfaceOptions),
      // macd(xValues, closeValues, axisAlignment, subChartModifiers, commonSubChartSurfaceOptions, config),
      ...indicators.map(item => indicatorsList[item.name](priceBars, item.containerId, item.position, item.period, item.drawLabels))
    ]
  })

  const subSurfaces = mainSurface.subCharts

  subSurfaces.forEach((subSurface, i) => {
    verticalGroup.addSurfaceToGroup(subSurface)

    const chartXAxis = subSurface.xAxes.get(0)
    chartXAxis.visibleRangeChanged.subscribe((data) => {
      const filteredSubSurfaces = subSurfaces.filter((item, index) => index !== i)
      filteredSubSurfaces.forEach(item => {
        const chartXAxisItem = item.xAxes.get(0)
        if (data) {
          chartXAxisItem.visibleRange = data.visibleRange
        }
      })
    })
  })

  // Add some trades to the chart using the Annotations API
  for (let i = 0; i < subSurfaces[0].renderableSeries.get(0).dataSeries.count(); i++) {
    // Every 1000th bar, add a buy annotation
    if (i % 1000 === 0) {
      subSurfaces[0].annotations.add(buyMarkerAnnotation(i, lowValues[i]))
    }
    // Every 1000th bar between buys, add a sell annotation
    if ((i + 500) % 1000 === 0) {
      subSurfaces[0].annotations.add(sellMarkerAnnotation(i, highValues[i]))
    }
  }

  // Resizing Logic
  const firstDividerElement = document.getElementById('dividerId' + 1)
  const secondDividerElement = document.getElementById('dividerId' + 2)
  const thirdDividerElement = document.getElementById('dividerId' + 3)
  let isDraggingFirst = false
  let isDraggingSecond = false
  let isDraggingThird = false
  let dragStartPosition: number
  const container = document.getElementById(containerId2)

  const baseHeight = container?.offsetHeight ?? 0
  const minPaneSize = 0.1

  const resizePanesFirst = (mouseYOffset: number) => {
    let newPosition = mouseYOffset / baseHeight

    if (newPosition < minPaneSize) {
      newPosition = minPaneSize
    }

    if (newPosition > subSurfaces[2].subPosition.y - minPaneSize) {
      newPosition = subSurfaces[2].subPosition.y - minPaneSize
    }
    firstDividerElement.style.top = `${newPosition * 100}%`
    subSurfaces[0].subPosition = new Rect(
      0,
      0,
      1,
      newPosition
    )
    subSurfaces[1].subPosition = new Rect(
      0,
      newPosition,
      1,
      subSurfaces[2].subPosition.y - newPosition
    )
  }

  const resizePanesSecond = (mouseYOffset: number) => {
    let newPosition = mouseYOffset / baseHeight

    if (newPosition > subSurfaces[3].subPosition.y - minPaneSize) {
      newPosition = subSurfaces[3].subPosition.y - minPaneSize
    }

    if (newPosition < subSurfaces[1].subPosition.y + minPaneSize) {
      newPosition = subSurfaces[1].subPosition.y + minPaneSize
    }
    secondDividerElement.style.top = `${newPosition * 100}%`
    subSurfaces[1].subPosition = new Rect(
      0,
      subSurfaces[1].subPosition.y,
      1,
      newPosition - subSurfaces[1].subPosition.y
    )
    subSurfaces[2].subPosition = new Rect(
      0,
      newPosition,
      1,
      subSurfaces[3].subPosition.y - newPosition
    )
  }

  const resizePanesThird = (mouseYOffset: number) => {
    let newPosition = mouseYOffset / baseHeight

    if (newPosition > 1 - minPaneSize) {
      newPosition = 1 - minPaneSize
    }

    if (newPosition < subSurfaces[2].subPosition.y + minPaneSize) {
      newPosition = subSurfaces[2].subPosition.y + minPaneSize
    }
    thirdDividerElement.style.top = `${newPosition * 100}%`
    subSurfaces[2].subPosition = new Rect(
      0,
      subSurfaces[2].subPosition.y,
      1,
      newPosition - subSurfaces[2].subPosition.y
    )
    subSurfaces[3].subPosition = new Rect(
      0,
      newPosition,
      1,
      1 - newPosition
    )
  }

  const mouseDownHandlerFirst = (event: MouseEvent) => {
    isDraggingFirst = true
    dragStartPosition = event.clientY
  }

  const mouseDownHandlerSecond = (event: MouseEvent) => {
    isDraggingSecond = true
    dragStartPosition = event.clientY
  }

  const mouseDownHandlerThird = (event: MouseEvent) => {
    isDraggingThird = true
    dragStartPosition = event.clientY
  }

  const mouseUpHandler = () => {
    if (!isDraggingFirst && !isDraggingSecond && !isDraggingThird) {
      return
    }

    isDraggingFirst = false
    isDraggingSecond = false
    isDraggingThird = false
  }

  const mouseMoveHandler = (event: MouseEvent) => {
    if (!isDraggingFirst && !isDraggingSecond && !isDraggingThird) {
      return
    }

    const rect = container?.getBoundingClientRect()
    const mouseYOffset = event.clientY - (rect?.top ?? 0)

    if (isDraggingFirst) {
      resizePanesFirst(mouseYOffset)
    } else if (isDraggingSecond) {
      resizePanesSecond(mouseYOffset)
    } else {
      resizePanesThird(mouseYOffset)
    }
  }

  const paneMouseDownHandler = (event: MouseEvent) => {
    event.preventDefault()
  }

  firstDividerElement.addEventListener('mousedown', mouseDownHandlerFirst)
  secondDividerElement.addEventListener('mousedown', mouseDownHandlerSecond)
  thirdDividerElement.addEventListener('mousedown', mouseDownHandlerThird)
  container.addEventListener('mousedown', paneMouseDownHandler)
  container.addEventListener('mouseup', mouseUpHandler)
  container.addEventListener('mousemove', mouseMoveHandler)
  container.addEventListener('mouseleave', mouseUpHandler)

  firstDividerElement.style.top = `${subSurfaces[1].subPosition.y * 100}%`
  secondDividerElement.style.top = `${subSurfaces[2].subPosition.y * 100}%`
  thirdDividerElement.style.top = `${subSurfaces[3].subPosition.y * 100}%`

  return { sciChartSurface: mainSurface, wasmContext }
}

/**
 * An example PaletteProvider applied to the volume column series. It will return green / red
 * fills and strokes when the main price data bar is up or down
 */

// Returns a CustomAnnotation that represents a buy marker arrow
// The CustomAnnotation supports SVG as content. Using Inkscape or similar you can create SVG content for annotations
const buyMarkerAnnotation = (x1: number, y1: number): CustomAnnotation => {
  return new CustomAnnotation({
    x1,
    y1,
    verticalAnchorPoint: EVerticalAnchorPoint.Top,
    horizontalAnchorPoint: EHorizontalAnchorPoint.Center,
    svgString:
      '<svg id="Capa_1" xmlns="http://www.w3.org/2000/svg">' +
      '<g transform="translate(-53.867218,-75.091687)">' +
      '<path style="fill:#1cb61c;fill-opacity:0.34117647;stroke:#00b400;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"' +
      'd="m 55.47431,83.481251 c 7.158904,-7.408333 7.158904,-7.408333 7.158904,-7.408333 l 7.158906,7.408333 H 66.212668 V 94.593756 H 59.053761 V 83.481251 Z"' +
      '/>' +
      '</g>' +
      '</svg>'
  })
}

// Returns a CustomAnnotation that represents a sell marker arrow
// The CustomAnnotation supports SVG as content. Using Inkscape or similar you can create SVG content for annotations
const sellMarkerAnnotation = (x1: number, y1: number): CustomAnnotation => {
  return new CustomAnnotation({
    x1,
    y1,
    verticalAnchorPoint: EVerticalAnchorPoint.Bottom,
    horizontalAnchorPoint: EHorizontalAnchorPoint.Center,
    svgString:
      '<svg id="Capa_1" xmlns="http://www.w3.org/2000/svg">' +
      '<g transform="translate(-54.616083,-75.548914)">' +
      '<path style="fill:#b22020;fill-opacity:0.34117648;stroke:#990000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"' +
      'd="m 55.47431,87.025547 c 7.158904,7.408333 7.158904,7.408333 7.158904,7.408333 L 69.79212,87.025547 H 66.212668 V 75.913042 h -7.158907 v 11.112505 z"' +
      '/>' +
      '</g>' +
      '</svg>'
  })
}
