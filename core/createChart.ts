import {
  EAutoRange,
  easing,
  ECoordinateMode,
  EDataSeriesType,
  EHorizontalAnchorPoint,
  EAnnotationLayer,
  EStrokePaletteMode,
  ESeriesType,
  ENumericFormat,
  EVerticalAnchorPoint,
  CursorModifier,
  CursorTooltipSvgAnnotation,
  DateTimeNumericAxis,
  EllipsePointMarker,
  FastBubbleRenderableSeries,
  FastCandlestickRenderableSeries,
  FastColumnRenderableSeries,
  FastLineRenderableSeries,
  FastMountainRenderableSeries,
  FastOhlcRenderableSeries,
  GradientParams,
  HorizontalLineAnnotation,
  type IPointMarkerPaletteProvider,
  type IPointMetadata,
  type IRenderableSeries,
  MouseWheelZoomModifier,
  NumberRange,
  NumericAxis,
  OhlcDataSeries,
  OhlcSeriesInfo,
  parseColorToUIntArgb,
  Point,
  SeriesInfo,
  SciChartOverview,
  SciChartSurface,
  TextAnnotation,
  type TPointMarkerArgb,
  XyzDataSeries,
  XyDataSeries,
  ZoomExtentsModifier,
  ZoomPanModifier,
  SciChartSubSurface,
  Rect,
  CategoryAxis,
  SmartDateLabelProvider,
  ELabelAlignment,
  calcAverageForArray,
  EXyDirection, RolloverModifier, SciChartVerticalGroup
} from 'scichart'
import { type TPriceBar } from "@/api/binance/restClient";
import { appTheme } from "@/lib/theme";
import { VolumePaletteProvider } from "@/core/VolumePalleteProvider";
import { ema } from '@/core/indicators/ema'
import { sma } from '@/core/indicators/sma'

export const LARGE_TRADE_THRESHOLD = 25_000;

export const createCandlestickChart = async (
  divChartId: string,
  divOverviewId: string,
  divChartId1: string,
) => {
  const verticalGroup = new SciChartVerticalGroup();

  const { sciChartSurface, wasmContext } = await SciChartSurface.create(
    divChartId,
    {
      theme: appTheme.SciChartJsTheme,
    }
  );

  const xAxis = new DateTimeNumericAxis(wasmContext);
  sciChartSurface.xAxes.add(xAxis);

  sciChartSurface.yAxes.add(
    new NumericAxis(wasmContext, {
      growBy: new NumberRange(0.1, 0.1),
      labelFormat: ENumericFormat.Decimal,
      labelPrecision: 2,
      labelPrefix: "$",
      autoRange: EAutoRange.Always,
    })
  );

  const Y_AXIS_VOLUME_ID = "Y_AXIS_VOLUME_ID";
  sciChartSurface.yAxes.add(
    new NumericAxis(wasmContext, {
      id: Y_AXIS_VOLUME_ID,
      growBy: new NumberRange(0, 4),
      isVisible: false,
      autoRange: EAutoRange.Always,
    })
  );

  const candleDataSeries = new OhlcDataSeries(wasmContext);
  const candlestickSeries = new FastCandlestickRenderableSeries(wasmContext, {
    dataSeries: candleDataSeries,
    stroke: appTheme.ForegroundColor, // used by cursorModifier below
    strokeThickness: 1,
    brushUp: appTheme.VividGreen + "77",
    brushDown: appTheme.MutedRed + "77",
    strokeUp: appTheme.VividGreen,
    strokeDown: appTheme.MutedRed,
  });
  sciChartSurface.renderableSeries.add(candlestickSeries);

  const ohlcSeries = new FastOhlcRenderableSeries(wasmContext, {
    dataSeries: candleDataSeries,
    stroke: appTheme.ForegroundColor, // used by cursorModifier below
    strokeThickness: 1,
    dataPointWidth: 0.9,
    strokeUp: appTheme.VividGreen,
    strokeDown: appTheme.MutedRed,
    isVisible: false,
  });
  sciChartSurface.renderableSeries.add(ohlcSeries);

  const volumeDataSeries = new XyDataSeries(wasmContext, {
    dataSeriesName: "Volume",
  });
  sciChartSurface.renderableSeries.add(
    new FastColumnRenderableSeries(wasmContext, {
      dataSeries: volumeDataSeries,
      strokeThickness: 0,
      yAxisId: Y_AXIS_VOLUME_ID,
      paletteProvider: new VolumePaletteProvider(
        candleDataSeries,
        appTheme.VividGreen + "77",
        appTheme.MutedRed + "77"
      ),
    })
  );

  const largeTradesDataSeries = new XyzDataSeries(wasmContext, {
    dataSeriesName: `Trades Size > $${LARGE_TRADE_THRESHOLD.toLocaleString()}`,
  });
  sciChartSurface.renderableSeries.add(
    new FastBubbleRenderableSeries(wasmContext, {
      dataSeries: largeTradesDataSeries,
      stroke: appTheme.VividGreen,
      pointMarker: new EllipsePointMarker(wasmContext, {
        width: 64,
        height: 64,
        opacity: 0.23,
        strokeThickness: 2,
      }),
      paletteProvider: new LargeTradesPaletteProvider(
        appTheme.VividGreen,
        appTheme.MutedRed
      ),
    })
  );

  sciChartSurface.chartModifiers.add(
    new ZoomExtentsModifier(),
    new ZoomPanModifier(),
    new MouseWheelZoomModifier(),
    new CursorModifier({
      crosshairStroke: appTheme.VividOrange,
      axisLabelFill: appTheme.VividOrange,
      tooltipLegendTemplate: getTooltipLegendTemplate,
    })
  );

  const sciChartOverview = await SciChartOverview.create(
    sciChartSurface,
    divOverviewId,
    {
      theme: appTheme.SciChartJsTheme,
      transformRenderableSeries: getOverviewSeries,
    }
  );

  const watermarkAnnotation = new TextAnnotation({
    x1: 0.5,
    y1: 0.5,
    xCoordinateMode: ECoordinateMode.Relative,
    yCoordinateMode: ECoordinateMode.Relative,
    horizontalAnchorPoint: EHorizontalAnchorPoint.Center,
    verticalAnchorPoint: EVerticalAnchorPoint.Center,
    opacity: 0.2,
    textColor: appTheme.ForegroundColor,
    fontSize: 48,
    fontWeight: "Bold",
    text: "",
    annotationLayer: EAnnotationLayer.BelowChart,
  });

  const latestPriceAnnotation = new HorizontalLineAnnotation({
    isHidden: true,
    strokeDashArray: [2, 2],
    strokeThickness: 1,
    axisFontSize: 13,
    axisLabelStroke: appTheme.ForegroundColor,
    showLabel: true,
  });
  sciChartSurface.annotations.add(latestPriceAnnotation);

  const updateLatestPriceAnnotation = (priceBar: TPriceBar) => {
    latestPriceAnnotation.isHidden = false;
    latestPriceAnnotation.y1 = priceBar?.close;
    latestPriceAnnotation.stroke =
      priceBar?.close > priceBar.open ? appTheme.VividGreen : appTheme.MutedRed;
    latestPriceAnnotation.axisLabelFill = latestPriceAnnotation.stroke;
  };

  verticalGroup.addSurfaceToGroup(sciChartSurface)

  const setData = async (
    symbolName: string,
    watermarkText: string,
    priceBars: TPriceBar[]
  ) => {
    console.log(
      `createCandlestickChart(): Setting data for ${symbolName}, ${priceBars.length} candles`
    );

    const xValues: number[] = [];
    const openValues: number[] = [];
    const highValues: number[] = [];
    const lowValues: number[] = [];
    const closeValues: number[] = [];
    const volumeValues: number[] = [];
    priceBars.forEach((priceBar: TPriceBar) => {
      xValues.push(priceBar.date);
      openValues.push(priceBar.open);
      highValues.push(priceBar.high);
      lowValues.push(priceBar.low);
      closeValues.push(priceBar.close);
      volumeValues.push(priceBar.volume);
    });

    // const dataSeries = new XyDataSeries(wasmContext, {
    //   dataSeriesName: `EMA(${13})`,
    //   xValues: ema(xValues, closeValues, {}, 13).xyData.xValues,
    //   yValues: ema(xValues, closeValues, {}, 13).xyData.yValues,
    // });
    // const lineSeries = new FastLineRenderableSeries(wasmContext, {
    //   stroke: appTheme.VividRed,
    //   strokeThickness: 1,
    //   dataSeries
    // });
    // sciChartSurface.renderableSeries.add(lineSeries);

    //TODO add macd indicator
    const drawRsiChart = async (rootElement: string | HTMLDivElement) => {
      const { wasmContext, sciChartSurface } = await SciChartSurface.create(rootElement, {
        disableAspect: true,
        theme: appTheme.SciChartJsTheme,
      });

      const chart3XAxis = new CategoryAxis(wasmContext, {
        autoRange: EAutoRange.Once,
        labelProvider: new SmartDateLabelProvider(),
      });
      sciChartSurface.xAxes.add(chart3XAxis);

      const yAxis = new NumericAxis(wasmContext, {
        autoRange: EAutoRange.Always,
        growBy: new NumberRange(0.1, 0.1),
        labelPrecision: 0,
        cursorLabelPrecision: 0,
        labelStyle: { alignment: ELabelAlignment.Right },
      });
      yAxis.labelProvider.numericFormat = ENumericFormat.Decimal;
      sciChartSurface.yAxes.add(yAxis);

      const RSI_PERIOD = 14;
      const rsiArray: number[] = [];
      const gainArray: number[] = [];
      const lossArray: number[] = [];
      rsiArray.push(NaN);
      gainArray.push(NaN);
      lossArray.push(NaN);
      for (let i = 1; i < xValues.length; i++) {
        const previousClose = closeValues[i - 1];
        const currentClose = closeValues[i];
        const gain = currentClose > previousClose ? currentClose - previousClose : 0;
        gainArray.push(gain);
        const loss = previousClose > currentClose ? previousClose - currentClose : 0;
        lossArray.push(loss);
        const relativeStrength =
          calcAverageForArray(gainArray, RSI_PERIOD) / calcAverageForArray(lossArray, RSI_PERIOD);
        const rsi = 100 - 100 / (1 + relativeStrength);
        rsiArray.push(rsi);
      }
      const rsiRenderableSeries = new FastLineRenderableSeries(wasmContext, {
        dataSeries: new XyDataSeries(wasmContext, {
          dataSeriesName: "RSI",
          xValues: xValues,
          yValues: rsiArray,
        }),
        stroke: appTheme.MutedBlue,
        strokeThickness: 2,
      });
      sciChartSurface.renderableSeries.add(rsiRenderableSeries);

      sciChartSurface.chartModifiers.add(new ZoomPanModifier({ xyDirection: EXyDirection.XDirection }));
      sciChartSurface.chartModifiers.add(new ZoomExtentsModifier({ xyDirection: EXyDirection.XDirection }));
      sciChartSurface.chartModifiers.add(new MouseWheelZoomModifier({ xyDirection: EXyDirection.XDirection }));
      sciChartSurface.chartModifiers.add(
        new RolloverModifier({
          modifierGroup: "cursorGroup",
          showTooltip: false,
          tooltipLegendTemplate: getTooltipLegendTemplate,
        })
      );

      verticalGroup.addSurfaceToGroup(sciChartSurface);

      return { wasmContext, sciChartSurface };
    };



    candleDataSeries.clear();
    candleDataSeries.appendRange(
      xValues,
      openValues,
      highValues,
      lowValues,
      closeValues
    );
    volumeDataSeries.clear();
    volumeDataSeries.appendRange(xValues, volumeValues);

    candleDataSeries.dataSeriesName = symbolName;

    watermarkAnnotation.text = watermarkText;
    updateLatestPriceAnnotation(priceBars[priceBars.length - 1]);
  };

  const onNewTrade = (
    priceBar: TPriceBar,
    tradeSize: number,
    lastTradeBuyOrSell: boolean
  ) => {
    const currentIndex = candleDataSeries.count() - 1;
    const getLatestCandleDate = candleDataSeries
      .getNativeXValues()
      .get(currentIndex);
    if (priceBar.date / 1000 === getLatestCandleDate) {
      candleDataSeries.update(
        currentIndex,
        priceBar.open,
        priceBar.high,
        priceBar.low,
        priceBar.close
      );
      volumeDataSeries.update(currentIndex, priceBar.volume);
    } else {
      candleDataSeries.append(
        priceBar.date / 1000,
        priceBar.open,
        priceBar.high,
        priceBar.low,
        priceBar.close
      );
      volumeDataSeries.append(priceBar.date / 1000, priceBar.volume);

      if (xAxis.visibleRange.max > getLatestCandleDate) {
        const dateDifference = priceBar.date / 1000 - getLatestCandleDate;
        const shiftedRange = new NumberRange(
          xAxis.visibleRange.min + dateDifference,
          xAxis.visibleRange.max + dateDifference
        );
        xAxis.animateVisibleRange(shiftedRange, 250, easing.inOutQuad);
      }
    }
    const tradeValue = tradeSize * priceBar.close;
    if (tradeValue > LARGE_TRADE_THRESHOLD) {
      const tradeValueNormalised = 20 * Math.log10(tradeValue) - 70;
      console.log(
        `Large trade: ${new Date(priceBar.date)}, price ${
          priceBar.close
        }, size ${lastTradeBuyOrSell ? "+" : "-"}$${tradeValue.toFixed(2)}`
      );
      largeTradesDataSeries.append(
        priceBar.date / 1000,
        priceBar.close,
        tradeValueNormalised,
        {
          isSelected: false,
          // @ts-ignore
          lastTradeBuyOrSell,
        }
      );
    }
    updateLatestPriceAnnotation(priceBar);
  };

  const setXRange = (startDate: Date, endDate: Date) => {
    console.log(
      `createCandlestickChart(): Setting chart range to ${startDate} - ${endDate}`
    );
    xAxis.visibleRange = new NumberRange(
      startDate.getTime() / 1000,
      endDate.getTime() / 1000
    );
  };

  const enableCandlestick = () => {
    candlestickSeries.isVisible = true;
    ohlcSeries.isVisible = false;
  };

  const enableOhlc = () => {
    candlestickSeries.isVisible = false;
    ohlcSeries.isVisible = true;
  };

  return {
    sciChartSurface,
    sciChartOverview,
    controls: { setData, onNewTrade, setXRange, enableCandlestick, enableOhlc },
  };
};
const getOverviewSeries = (defaultSeries: IRenderableSeries) => {
  if (defaultSeries.type === ESeriesType.CandlestickSeries) {
    return new FastMountainRenderableSeries(
      defaultSeries.parentSurface.webAssemblyContext2D,
      {
        dataSeries: defaultSeries.dataSeries,
        fillLinearGradient: new GradientParams(
          new Point(0, 0),
          new Point(0, 1),
          [
            { color: appTheme.VividSkyBlue + "77", offset: 0 },
            { color: "Transparent", offset: 1 },
          ]
        ),
        stroke: appTheme.VividSkyBlue,
      }
    );
  }
  return undefined;
};

const getTooltipLegendTemplate = (
  seriesInfos: SeriesInfo[],
  svgAnnotation: CursorTooltipSvgAnnotation
) => {
  let outputSvgString = "";

  seriesInfos.forEach((seriesInfo, index) => {
    const y = 20 + index * 20;
    const textColor = seriesInfo.stroke;
    let legendText = seriesInfo.formattedYValue;
    let separator = ":";
    if (seriesInfo.dataSeriesType === EDataSeriesType.Ohlc) {
      const o = seriesInfo as OhlcSeriesInfo;
      legendText = `Open=${o.formattedOpenValue} High=${o.formattedHighValue} Low=${o.formattedLowValue} Close=${o.formattedCloseValue}`;
    }
    if (seriesInfo.dataSeriesType === EDataSeriesType.Xyz) {
      legendText = "";
      separator = "";
    }
    outputSvgString += `<text x="8" y="${y}" font-size="10" font-family="Verdana" fill="${textColor}">
            ${seriesInfo.seriesName}${separator} ${legendText}
        </text>`;
  });

  return `<svg width="100%" height="100%">
                ${outputSvgString}
            </svg>`;
};

class LargeTradesPaletteProvider implements IPointMarkerPaletteProvider {
  private readonly upColorArgb: number;
  private readonly downColorArgb: number;

  constructor(upColor: string, downColor: string) {
    this.upColorArgb = parseColorToUIntArgb(upColor);
    this.downColorArgb = parseColorToUIntArgb(downColor);
  }

  overridePointMarkerArgb(
    xValue: number,
    yValue: number,
    index: number,
    opacity?: number,
    metadata?: IPointMetadata
  ): TPointMarkerArgb {
    // @ts-ignore
    const tradeColor = metadata?.lastTradeBuyOrSell
      ? this.upColorArgb
      : this.downColorArgb;
    return { fill: tradeColor, stroke: tradeColor };
  }

  strokePaletteMode: EStrokePaletteMode = EStrokePaletteMode.SOLID;

  onAttached(parentSeries: IRenderableSeries): void {}

  onDetached(): void {}
}
