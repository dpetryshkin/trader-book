import { EAutoRange, EAxisType, ESeriesType, NumberRange, Rect } from 'scichart'
import { appTheme } from '@/lib/theme'
import { ema } from '@/core/indicators/ema'
import { sma } from '@/core/indicators/sma'
import { volume } from '@/core/indicators/volume'
import { subChartWrapper1 } from '@/core/sci'

export function chart(
  xValues: number[],
  openValues: number[],
  highValues: number[],
  lowValues: number[],
  closeValues: number[],
  volumeValues: number[],
  axisAlignment: any,
  subChartModifiers: any,
  config: any,
  commonSubChartSurfaceOptions: any
) {
  return {
    surface: {
      ...commonSubChartSurfaceOptions,
      position: new Rect(0, 0, 1, 0.5),
      id: subChartWrapper1,
      subChartContainerId: subChartWrapper1
    },
    xAxes: {
      type: EAxisType.CategoryAxis,
      options: {
        drawLabels: false,
        autoRange: EAutoRange.Once,
        maxAutoTicks: 20,
        useNativeText: false,
        minorsPerMajor: 3
      }
    },
    yAxes: [
      {
        type: EAxisType.NumericAxis,
        options: {
          maxAutoTicks: 10,
          autoRange: EAutoRange.Always,
          growBy: new NumberRange(0.3, 0.11),
          labelPrecision: 2,
          labelPrefix: '$',
          axisAlignment,
          drawMinorGridLines: false
        }
      },
      {
        type: EAxisType.NumericAxis,
        options: {
          id: 'yAxis2',
          isVisible: false,
          autoRange: EAutoRange.Always,
          growBy: new NumberRange(0, 3)
        }
      }
    ],
    series: [
      {
        type: ESeriesType.CandlestickSeries,
        ohlcData: {
          dataSeriesName: 'OHLC',
          xValues,
          openValues,
          highValues,
          lowValues,
          closeValues
        },
        options: {
          brushUp: config.upCol + config.opacity,
          brushDown: config.downCol + config.opacity,
          strokeUp: config.upCol,
          strokeDown: config.downCol
        }
      },
      ema(xValues, closeValues, { stroke: appTheme.VividRed, strokeThickness: 1 }, 13),
      sma(xValues, closeValues, { stroke: appTheme.MutedOrange, strokeThickness: 2 }, 26),
      volume(xValues, openValues, closeValues, volumeValues, config)
    ],
    modifiers: subChartModifiers,
    sharedData: {}
  }
}