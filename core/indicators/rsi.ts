import {
  EAutoRange,
  EAxisAlignment,
  EAxisType,
  EChart2DModifierType,
  ESeriesType,
  EXyDirection,
  type I2DSubSurfaceOptions,
  NumberRange,
  Rect,
  Thickness
} from 'scichart'
import { appTheme } from '@/lib/theme'
import { rsiData } from '@/indicatorsData/rsiData'
import { multiPaneData } from '@/lib/multiPaneData'
import { getTooltipLegendTemplate } from '@/utils/legends/TooltipLegend'
import type { Position } from '@/interfaces/indicator'
import type { TPriceBar } from '@/api/binance/restClient'

export function rsi(priceBars: TPriceBar[], containerId: string, position: Position, RSI_PERIOD: number = 26, drawLabels: boolean): any {
  const xValues: number[] = [];
  const openValues: number[] = [];
  const highValues: number[] = [];
  const lowValues: number[] = [];
  const closeValues: number[] = [];
  const volumeValues: number[] = [];
  priceBars.forEach((priceBar: TPriceBar) => {
    xValues.push(priceBar.date);
    openValues.push(priceBar.open);
    highValues.push(priceBar.high);
    lowValues.push(priceBar.low);
    closeValues.push(priceBar.close);
    volumeValues.push(priceBar.volume);
  });
  const modifiers = [
    { type: EChart2DModifierType.ZoomPan, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.PinchZoom, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.ZoomExtents, options: { xyDirection: EXyDirection.XDirection } },
    { type: EChart2DModifierType.MouseWheelZoom, options: { xyDirection: EXyDirection.XDirection } },
    {
      type: EChart2DModifierType.Rollover,
      options: { modifierGroup: 'cursorGroup', showTooltip: false, tooltipLegendTemplate: getTooltipLegendTemplate }
    }
  ]
  const axisAlignment = EAxisAlignment.Right
  const commonSubChartSurfaceOptions: I2DSubSurfaceOptions = {
    subChartPadding: Thickness.fromNumber(10),
    isTransparent: false,
    theme: appTheme.SciChartJsTheme
  }
  const { rsiArray } = rsiData(xValues, closeValues, RSI_PERIOD)

  return {
    surface: {
      ...commonSubChartSurfaceOptions,
      position: new Rect(position.x, position.y, position.width, position.height),
      subChartPadding: Thickness.fromNumber(10),
      id: containerId,
      isTransparent: false,
      subChartContainerId: containerId
    },
    xAxes: {
      type: EAxisType.CategoryAxis,
      options: {
        drawLabels,
        drawMajorTickLines: true,
        drawMinorTickLines: false,
        useNativeText: false,
        minorsPerMajor: 3
      }
    },
    yAxes: {
      type: EAxisType.NumericAxis,
      options: {
        labelPrecision: 0,
        maxAutoTicks: 6,
        autoRange: EAutoRange.Always,
        growBy: new NumberRange(0.1, 0.1),
        axisAlignment,
        drawMinorGridLines: false
      }
    },
    series: {
      type: ESeriesType.LineSeries,
      xyData: {
        dataSeriesName: `RSI(${RSI_PERIOD})`,
        xValues,
        yValues: rsiArray
      },
      options: {
        stroke: appTheme.VividSkyBlue,
        strokeThickness: 1
      }
    },
    modifiers
  }
}
