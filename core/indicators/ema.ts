import { EDataFilterType, EDataSeriesField, ESeriesType } from 'scichart'
import { emaData } from '@/indicatorsData/emaData'

export function ema(xValues: number[], closeValues: number[], options: any, period: number = 13) {
  const yValues = emaData(closeValues.map((item: any, i: number) => {
    return {
      x: new Date(xValues[i] * 1000),
      y: item
    }
  }), period).map(item => item.y)

  return {
    type: ESeriesType.LineSeries,
    xyData: {
      dataSeriesName: `EMA(${period})`,
      xValues,
      yValues,
      filter: {
        type: EDataFilterType.XyMovingAverage,
        options: {
          length: period,
          field: EDataSeriesField.Low
        }
      }
    },
    options
  }
}