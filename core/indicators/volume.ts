import { ESeriesType } from 'scichart'
import { VolumePaletteProvider } from '@/core/providers/VolumePalleteProvider'

export function volume(xValues: number[], openValues: number[], closeValues: number[], volumeValues: number[], config: any) {
  return {
    type: ESeriesType.ColumnSeries,
      xyData: {
    dataSeriesName: 'Volume',
      xValues,
      yValues: volumeValues
  },
    options: {
      yAxisId: 'yAxis2',
        dataPointWidth: 0.5,
        strokeThickness: 0,
        paletteProvider: new VolumePaletteProvider(
        openValues,
        closeValues,
        config.upCol + config.opacity,
        config.downCol + config.opacity
      )
    }
  }
}