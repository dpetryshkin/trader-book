import { rsi } from '@/core/indicators/rsi'
import { macd } from '@/core/indicators/macd'

const indicatorsList = {
  rsi,
  macd
}

export type TIndicatorsList = keyof typeof indicatorsList

export default indicatorsList