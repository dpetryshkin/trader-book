import { EDataFilterType, EDataSeriesField, ESeriesType } from 'scichart'
import { smaData } from '@/indicatorsData/smaData'

export function sma(xValues: number[], closeValues: number[], options: any, period: number = 26) {
  const yValues = smaData(closeValues.map((item: any, i: number) => {
    return {
      x: new Date(xValues[i] * 1000),
      y: item
    }
  }), period).map(item => item.y)

  return {
    type: ESeriesType.LineSeries,
    xyData: {
      dataSeriesName: `SMA(${period})`,
      xValues,
      yValues,
      filter: {
        type: EDataFilterType.XyMovingAverage,
        options: {
          length: period,
          field: EDataSeriesField.Low
        }
      }
    },
    options
  }
}