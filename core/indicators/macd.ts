import { EAutoRange, EAxisType, ESeriesType, NumberRange, Rect } from 'scichart'
import { MacdHistogramPaletteProvider } from '@/core/providers/MacdHistogramPalleteProvider'
import { subChartWrapper2 } from '@/core/sci'
import { macdData } from '@/indicatorsData/macdData'

export function macd (xValues: any, closeValues: any, axisAlignment: any, subChartModifiers: any, commonSubChartSurfaceOptions: any, config: any ) {
  const { macdArray, signalArray, divergenceArray } = macdData(xValues, closeValues)

  return {
    surface: {
    ...commonSubChartSurfaceOptions,
        position: new Rect(0, 0.5, 1, 0.3),
        subChartContainerId: subChartWrapper2,
        id: 'subChart2'
    },
    xAxes: [
      {
        type: EAxisType.CategoryAxis,
        options: {
          drawLabels: false,
          drawMajorTickLines: false,
          drawMinorTickLines: false,
          useNativeText: false,
          minorsPerMajor: 3
        }
      }
    ],
      yAxes: [
    {
      type: EAxisType.NumericAxis,
      options: {
        maxAutoTicks: 6,
        autoRange: EAutoRange.Always,
        growBy: new NumberRange(0.1, 0.1),
        axisAlignment,
        labelPrecision: 2,
        drawMinorGridLines: false
      }
    }
  ],
    series: [
    {
      type: ESeriesType.BandSeries,
      xyyData: {
        dataSeriesName: 'MACD',
        xValues,
        yValues: signalArray,
        y1Values: macdArray
      },
      options: {
        stroke: config.downCol,
        strokeY1: config.upCol,
        fill: config.upCol + config.opacity,
        fillY1: config.downCol + config.opacity
      }
    },
    {
      type: ESeriesType.ColumnSeries,
      xyData: {
        dataSeriesName: 'Divergence',
        xValues,
        yValues: divergenceArray
      },
      options: {
        dataPointWidth: 0.5,
        paletteProvider: new MacdHistogramPaletteProvider(config.upCol + config.opacity, config.downCol + config.opacity)
      }
    }
  ],
    modifiers: subChartModifiers
  }
}