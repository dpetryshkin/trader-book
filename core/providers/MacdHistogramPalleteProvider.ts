// tslint:disable-next-line: max-classes-per-file
import {
  BasePaletteProvider, EFillPaletteMode,
  EStrokePaletteMode,
  type IFillPaletteProvider,
  type IStrokePaletteProvider, parseColorToUIntArgb
} from 'scichart'

export class MacdHistogramPaletteProvider extends BasePaletteProvider implements IStrokePaletteProvider, IFillPaletteProvider {
  public readonly strokePaletteMode: EStrokePaletteMode = EStrokePaletteMode.SOLID
  public readonly fillPaletteMode: EFillPaletteMode = EFillPaletteMode.SOLID
  private aboveZeroArgb: number
  private belowZeroArgb: number

  constructor(aboveZeroColor: string, belowZeroColor: string) {
    super()

    this.aboveZeroArgb = parseColorToUIntArgb(aboveZeroColor)
    this.belowZeroArgb = parseColorToUIntArgb(belowZeroColor)
  }

  overrideFillArgb(xValue: number, yValue: number, index: number): number {
    return yValue >= 0 ? this.aboveZeroArgb : this.belowZeroArgb
  }

  overrideStrokeArgb(xValue: number, yValue: number, index: number): number {
    return this.overrideFillArgb(xValue, yValue, index)
  }
}
