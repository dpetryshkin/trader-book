import {
  BasePaletteProvider,
  EFillPaletteMode,
  EStrokePaletteMode,
  type IFillPaletteProvider,
  type IStrokePaletteProvider,
  parseColorToUIntArgb
} from 'scichart'

export class VolumePaletteProvider extends BasePaletteProvider implements IStrokePaletteProvider, IFillPaletteProvider {
  public readonly strokePaletteMode: EStrokePaletteMode = EStrokePaletteMode.SOLID
  public readonly fillPaletteMode: EFillPaletteMode = EFillPaletteMode.SOLID
  private openValues: number[]
  private closeValues: number[]
  private volumeUpArgb: number
  private volumnDownArgb: number

  constructor(openValues: number[], closeValues: number[], volumeUpColor: string, volumeDownColor: string) {
    super()
    this.openValues = openValues
    this.closeValues = closeValues
    this.volumeUpArgb = parseColorToUIntArgb(volumeUpColor)
    this.volumnDownArgb = parseColorToUIntArgb(volumeDownColor)
  }

  overrideFillArgb(xValue: number, yValue: number, index: number): number {
    const open = this.openValues[index]
    const close = this.closeValues[index]

    return close >= open ? this.volumeUpArgb : this.volumnDownArgb
  }

  overrideStrokeArgb(xValue: number, yValue: number, index: number): number {
    return this.overrideFillArgb(xValue, yValue, index)
  }
}
