import { EDataSeriesType, type SeriesInfo, type SvgAnnotationBase } from 'scichart'

export const getTooltipLegendTemplate = (seriesInfos: SeriesInfo[], svgAnnotation: SvgAnnotationBase) => {
  let outputSvgString = ''

  // Foreach series there will be a seriesInfo supplied by SciChart. This contains info about the series under the house
  seriesInfos.forEach((seriesInfo, index) => {
    const y = 20 + index * 20
    const textColor = seriesInfo.stroke
    let legendText = seriesInfo.formattedYValue
    if (seriesInfo.dataSeriesType === EDataSeriesType.Ohlc) {
      const o = seriesInfo as any
      legendText = `Open=${o.formattedOpenValue} High=${o.formattedHighValue} Low=${o.formattedLowValue} Close=${o.formattedCloseValue}`
    }
    outputSvgString += `<text x='8' y='${y}' font-size='13' font-family='Verdana' fill='#ffffff'>
            ${seriesInfo.seriesName}: ${legendText}
        </text>`
  })

  return `<svg width='100%' height='100%'>
                ${outputSvgString}
            </svg>`
}