export function SMA(dps: any[], count: number) {
  let avg = function(dps: any[]) {
    let sum = 0, count = 0, val
    for (let i = 0; i < dps.length; i++) {
      val = dps[i].y[3]
      sum += val
      count++
    }
    return sum / count
  }
  let result = [], val
  count = count || 5
  for (let i = 0; i < count; i++)
    result.push({ x: dps[i].x, y: null })
  for (let i = count - 1, len = dps.length; i < len; i++) {
    val = avg(dps.slice(i - count + 1, i))
    if (isNaN(val))
      result.push({ x: dps[i].x, y: null })
    else
      result.push({ x: dps[i].x, y: val })
  }
  return result
}