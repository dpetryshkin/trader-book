export function EMA(dps: any[], count: number) {
  let k = 2 / (count + 1)
  let emaDps = [{ x: dps[0].x, y: dps[0].y.length ? dps[0].y[3] : dps[0].y }]
  for (let i = 1; i < dps.length; i++) {
    emaDps.push({ x: dps[i].x, y: (dps[i].y.length ? dps[i].y[3] : dps[i].y) * k + emaDps[i - 1].y * (1 - k) })
  }
  return emaDps
}