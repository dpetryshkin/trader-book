import { EMA } from '@/utils/indicators/ema'

export function MACD(dps: any[]) {
  let ema12 = EMA(dps, 12),
    ema26 = EMA(dps, 26),
    macd = []
  for (let i = 0; i < ema12.length; i++) {
    macd.push({ x: ema12[i].x, y: (ema12[i].y - ema26[i].y) })
  }

  const hist = macd.map((item, i, arr) => {
    return {
      x: item.x,
      y: item.y - EMA(arr, 9)[i].y
    }
  })

  return {
    macd,
    hist,
    ema9: EMA(macd, 9)
  }
}