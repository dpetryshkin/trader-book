import { calcAverageForArray } from 'scichart'

export const rsiData = (xValues: number[], closeValues: number[], RSI_PERIOD: number = 14 ) => {
  const rsiArray: number[] = []
  const gainArray: number[] = []
  const lossArray: number[] = []
  rsiArray.push(NaN)
  gainArray.push(NaN)
  lossArray.push(NaN)
  for (let i = 1; i < xValues.length; i++) {
    const previousClose = closeValues[i - 1]
    const currentClose = closeValues[i]
    const gain = currentClose > previousClose ? currentClose - previousClose : 0
    gainArray.push(gain)
    const loss = previousClose > currentClose ? previousClose - currentClose : 0
    lossArray.push(loss)
    const relativeStrength =
      calcAverageForArray(gainArray, RSI_PERIOD) / calcAverageForArray(lossArray, RSI_PERIOD)
    const rsi = 100 - 100 / (1 + relativeStrength)
    rsiArray.push(rsi)
  }

  return { rsiArray }
}
