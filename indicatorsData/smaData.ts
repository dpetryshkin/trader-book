export function smaData(dps: any[], count: number) {
  let avg = function(dps: any[]) {
    let sum = 0, count = 0, val
    for (let i = 0; i < dps.length; i++) {
      val = dps[i].y
      sum += val
      count++
    }
    return sum / count
  }
  let result = [], val
  count = count || 5
  for (let i = 0; i < count; i++) {
    val = avg(dps.slice(0, i))
    result.push({ x: dps[i].x, y: val })
  }

  for (let i = count, len = dps.length; i < len; i++) {
    val = avg(dps.slice(i - count + 1, i))
    if (isNaN(val)) {
      result.push({ x: dps[i].x, y: NaN })
    } else
    result.push({ x: dps[i].x, y: val })
  }
  return result
}