import { calcAverageForArray } from 'scichart'

export const macdData = (xValues: number[], closeValues: number[]) => {
  const macdArray: number[] = []
  const signalArray: number[] = []
  const divergenceArray: number[] = []
  for (let i = 0; i < xValues.length; i++) {
    const maSlow = calcAverageForArray(closeValues, 12, i)
    const maFast = calcAverageForArray(closeValues, 25, i)
    const macd = maSlow - maFast
    macdArray.push(macd)
    const signal = calcAverageForArray(macdArray, 9, i)
    signalArray.push(signal)
    const divergence = macd - signal
    divergenceArray.push(divergence)
  }

  return { macdArray, signalArray, divergenceArray }
}