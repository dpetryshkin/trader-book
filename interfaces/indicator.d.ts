import type { TIndicatorsList } from '@/core/indicators'

export interface IndicatorData {
  name: TIndicatorsList;
  period: number;
  containerId: string;
  position: Position;
  drawLabels: boolean;
}

export interface Position {
  x: number;
  y: number;
  width: number;
  height: number;
}