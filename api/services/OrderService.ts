import { $api } from '@/api/http/common'
import type { CreateOrderData, GetOrderData, GetOrdersArgs, GetOrdersData } from '@/interfaces/order'

export const OrderService = {
  async getOrders(args?: GetOrdersArgs): Promise<GetOrdersData> {
    const url = 'orders'
    return await $api.get(url, args)
  },

  async createOrder(data: CreateOrderData): Promise<GetOrderData> {
    const url = 'orders'
    return await $api.post(url, data)
  },

  async getOrdersHistory(): Promise<any> {
    const url = 'orders/history'
    return await $api.get(url)
  },
}
