import { OrderService } from '@/api/services/OrderService'
import { AuthService } from '@/api/services/AuthService'
import { DepositService } from '@/api/services/DepositService'
import { StockService } from '@/api/services/StockService'

export default { ...AuthService, ...OrderService, ...DepositService, ...StockService }